﻿using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Http;

namespace MJTech.AspNetCore.Authentication.WechatMp
{
    public class WeChatMpOptions : OAuthOptions
	{
		
		public WeChatMpOptions()
		{
			base.CallbackPath = new PathString("/signin-wechat");
			this.StateAddition = "#wechat_redirect";
			base.AuthorizationEndpoint = WeChatMpAuthenticationScheme.AuthorizationEndpoint;
			base.TokenEndpoint = WeChatMpAuthenticationScheme.TokenEndpoint;
			base.UserInformationEndpoint = WeChatMpAuthenticationScheme.UserInformationEndpoint;
			this.WeChatScope = this.InfoScope;
		}

		
		public string AppId
		{
			set
			{
				ClientId = value;
			}
		}

		public string AppSecret
		{
			set
			{
				ClientSecret = value;
			}
		}

		
		public string StateAddition { get; set; }

	
		public string WeChatScope { get; set; }

		
		public string LoginScope = "snsapi_login";

		
		public string BaseScope = "snsapi_base";

		
		public string InfoScope = "snsapi_userinfo";
	}
}
